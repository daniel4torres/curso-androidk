# Avisos

## Avisos para el 11 de Junio de 2019

* Instalar Android Studio
* Instalar un emulador de télefono (AVD), para lo cual pueden ayudarse del siguiente [video](https://youtu.be/KGu_LlkEQfw)
  * Realizar el hola mundo y cargarlo en su AVD para asegurarse de que funciona.
* Descargar este repositorio.

